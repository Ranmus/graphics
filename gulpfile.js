// Required libraries
const gulp = require('gulp'),
    path = require('path'),
    $ = require('gulp-load-plugins')();

// Resolve root directory
const root = path.resolve(__dirname);

// Tasks configurations
const tasks = {
    webserver: {
        host: '127.0.0.1',
        port: 7890,
        root: root,
        livereload: true
    },
    css: {
        source: `${root}/assets/css/**/*.css`
    },
    js: {
        source: `${root}/assets/js/**/*.js`
    },
    html: {
        source: `${root}/**/*.html`
    }
};

/**
 * Default task
 */
gulp.task('default', ['help'], () => {});

/**
 * Help task
 */
gulp.task('help', () => {
    $.util.log(`
        - help - show this help
        - webserver - run webserver
        - develop - developer mode (+watch, +webserver)
    `);
});

/**
 * Watch task
 */
gulp.task('watch', () => {
    const logger = ({path}) => {
        $.util.log(`changed: ${path}`);
        gulp.src(path)
            .pipe($.wait(500))
            .pipe($.connect.reload());
    };

    gulp.watch(tasks.js.source).on('change', logger);
    gulp.watch(tasks.css.source).on('change', logger);
    gulp.watch(tasks.html.source).on('change', logger);
});

/**
 * Webserver task
 */
gulp.task('webserver', () => {
    const {host, port} = tasks.webserver,
    uri = `http://${host}:${port}`;

    $.connect.server(tasks.webserver);

    gulp.src(__filename)
        .pipe($.open({uri}));
});

/**
 * Develop task
 */
gulp.task('develop', ['webserver', 'watch'], () => {});
