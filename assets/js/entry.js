/**
 * Get element by id helper function
 * @param id
 * @returns {Element}
 */
const getById = (id) => {
    return document.getElementById(id);
};

/**
 * Set text helper function
 * @param element
 * @param text
 */
const setText = (element, text) => {
    element.innerHTML = text;
};

/**
 * Random color generator
 * @returns {string}
 */
function generateRandomColor() {
    const letters = '0123456789abcdef'.split('');
    let color = '#';

    for (let i = 0; i < 6; i++) {
        color += letters[Math.random() * 16 | 0];
    }

    return color;
}

/**
 * Initializes all magic
 */
window.onload = function() {

    const config = {
        context: localStorage.getItem('context') || '2d',
        target: document.querySelector('#canvas'),
        width: 1100,
        height: 500,
        spritesNumber: 10
    };

    // Images manager
    const imagesManager = ImagesManager.create();

    // Graphics manager
    const graphicsManager = new GraphicsManager(config);

    // Set layers
    const mainLayer = graphicsManager.createLayer().setFill('#009');
    //const hudLayer = graphicsManager.createLayer().setFill('red');
    //graphicsManager.setLayersOrder(mainLayer, hudLayer);

    // Elements
    const ui = {
        fps: getById('fps'),
        state: getById('state'),
        start: getById('start'),
        stop: getById('stop'),
        sprites: getById('sprites'),
        sp: getById('sprites-p'),
        sm: getById('sprites-m'),
        sn: getById('sprites-n'),
        sc: getById('set-canvas'),
        sw: getById('set-webgl')
    };

    let interval = null;
    var run;

    // Start button
    const start = () => {
        if (graphicsManager.isRunning() === false) {
            graphicsManager.start();
            ui.state.innerHTML = 'started';
            interval = setInterval(run, 1000);
        }
    };

    // Stop button
    const stop = () => {
        if (graphicsManager.isRunning()) {
            graphicsManager.stop();
            ui.state.innerHTML = 'stopped';
            ui.fps.innerHTML = '-';
            clearInterval(run);
        }
    };

    ui.start.onclick = start;
    ui.stop.onclick = stop;

    const restartSprites = (number) => {
        const old = mainLayer.getSprites();
        const sprites = createSprites(number);
        old.map(s => { mainLayer.removeSprite(s)});
        sprites.map(s => mainLayer.addSprite(s));
        ui.sprites.innerHTML = number;
    };

    const add = () => {
        const sprites = createSprites(config.spritesNumber);
        sprites.map(s => mainLayer.addSprite(s));
        ui.sprites.innerHTML = mainLayer.getSprites().length;
    };

    const remove = () => {
        const sprites = mainLayer.getSprites();
        for (let i = 0; i < config.spritesNumber; i++) {
            mainLayer.removeSprite(sprites[i]);
        }

        ui.sprites.innerHTML = mainLayer.getSprites().length;
    };

    ui.sp.onclick = add;
    ui.sm.onclick = remove;

    ui.sn.value = config.spritesNumber;
    ui.sn.onchange = () => {
        let number = Number(ui.sn.value);
        number = Math.max(0, number|0);

        config.spritesNumber = number;
    };

    ui.sc.onclick = () => {
        localStorage.setItem('context', '2d');
        location.reload();
    };

    ui.sw.onclick = () => {
        localStorage.setItem('context', 'webgl');
        location.reload();
    };

    if (config.context === '2d') {
        ui.sc.style.backgroundColor = '#050';
    } else {
        ui.sw.style.backgroundColor = '#050';
    }

    // Sprites generator
    const createSprites = (number) => {
        const sprites = [];
        const image = imagesManager.getSelectedImage();
        const dim = {
            w: 50,
            h: 50
        };

        if (image !== null) {
            dim.w = image.width;
            dim.h = image.height;
        }

        for (let i = 0; i < number ; i++) {
            const sprite = new Sprite({
                x: (Math.random() * (config.width - dim.w)) | 0,
                y: (Math.random() * (config.height - dim.h)) | 0,
                w: dim.w,
                h: dim.h
            });

            sprite.setColor({
                r: Math.random() * 255 | 0,
                g: Math.random() * 255 | 0,
                b: Math.random() * 255 | 0,
                a: 1
            });

            sprite.speed = {
                x: ((Math.random() * 20 - 10) | 0) / 2,
                y: ((Math.random() * 20 - 10) | 0) / 2
            };

            if (image) {
                sprite.setImage(image);
            }

            sprites.push(sprite);
        }

        return sprites;
    };

    graphicsManager.addCallback(() => {
        const sprites = mainLayer.getSprites();

        for (const sprite of sprites) {
            sprite.x += sprite.speed.x;
            sprite.y += sprite.speed.y;

            if (sprite.x < 0) {
                sprite.x *= -1;
                sprite.speed.x *= -1;
            }
            if (sprite.y < 0) {
                sprite.y *= -1;
                sprite.speed.y *= -1;
            }

            if (sprite.x > config.width - sprite.w) {
                sprite.x = config.width - sprite.w;
                sprite.speed.x *= -1;
            }
            if (sprite.y > config.height - sprite.h) {
                sprite.y = config.height - sprite.h;
                sprite.speed.y *= -1;
            }
        }
    });

    run = () => {
        if (graphicsManager.isRunning()) {
            ui.fps.innerHTML = graphicsManager.getFPS();
        }
    };

    //start();
    //add();
};
