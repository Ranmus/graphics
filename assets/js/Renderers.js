/**
 * Base renderer
 * Acts as abstract class
 */
class BaseRenderer {
    constructor(ctx) {
        this._ctx = ctx;
    }

    set ctx(ctx) {
        this._ctx = ctx;
        return this;
    }

    get ctx() {
        return this._ctx;
    }

    set fillColor(color) {
        this._fillColor = color;
    }

    get fillColor() {
        return this._fillColor;
    }
}

/**
 * Canvas renderer
 */
class CanvasRenderer extends BaseRenderer {
    constructor(canvas) {
        const ctx = canvas.getContext('2d');
        super(ctx);
    }

    clear(x,y,w,h) {
        this._ctx.fillStyle = this.fillColor;
        this._ctx.clearRect(x,y,w,h);
    }

    fill(x,y,w,h) {
        this._ctx.fillStyle = this.fillColor;
        this._ctx.fillRect(x,y,w,h);
    }

    drawSprite(sprite) {
        const ctx = this._ctx;
        const {x,y,w,h} = sprite.getCoords();

        if (sprite.hasImage()) {
            ctx.drawImage(sprite.image,x,y,w,h);
        } else {
            ctx.fillStyle = sprite.getCSSColor();
            ctx.fillRect(x,y,w,h);
        }
    }

    preDraw() {

    }

    postDraw() {

    }
}

/**
 * WebGL renderer
 */
class WebGLRenderer extends BaseRenderer {
    constructor(canvas) {
        const ctx = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');
        super(ctx);

        const gl = this.gl = ctx;
        gl.viewportWidth = canvas.width;
        gl.viewportHeight = canvas.height;

        this.w = canvas.width;
        this.h = canvas.h;

        const fs = gl.createShader(gl.FRAGMENT_SHADER);
        const vs = gl.createShader(gl.VERTEX_SHADER);

        gl.shaderSource(fs, `
            precision mediump float;
            
            uniform vec4 u_color;
            
            void main() {
                gl_FragColor = u_color;
            }
        `);

        gl.shaderSource(vs, `
            attribute vec2 a_position;

            void main() {
                gl_Position = vec4(a_position, 0.0, 1.0);
            }
        `);
/*
        gl.shaderSource(vs,
        `
            attribute vec2 a_position;
 
            uniform vec2 u_resolution;
            uniform vec2 u_translation;
 
            void main() {
               vec2 position = a_position + u_translation;
               vec2 zeroToOne = position / u_resolution;
             
               vec2 zeroToTwo = zeroToOne * 2.0;
               vec2 clipSpace = zeroToTwo - 1.0;
             
               gl_Position = vec4(clipSpace, 0, 1);
            }
        `);*/

        gl.compileShader(fs);
        gl.compileShader(vs);

        if (!gl.getShaderParameter(fs, gl.COMPILE_STATUS)) {
            console.log(gl.getShaderInfoLog(fs));
        }

        if (!gl.getShaderParameter(vs, gl.COMPILE_STATUS)) {
            console.log(gl.getShaderInfoLog(vs));
        }

        const sp = this.sp = gl.createProgram();
        gl.attachShader(sp, vs);
        gl.attachShader(sp, fs);
        gl.linkProgram(sp);

        if (!gl.getProgramParameter(sp, gl.LINK_STATUS)) {
            console.log('shaders not linked');
        }

        gl.useProgram(sp);
        this.pl = gl.getAttribLocation(sp, 'a_position');
        this.color = gl.getUniformLocation(sp, 'u_color');
        this.location = gl.getUniformLocation(sp, 'u_translation');

        this.buffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer);
        gl.enableVertexAttribArray(this.pl);
        gl.vertexAttribPointer(this.pl, 2, gl.FLOAT, false, 0, 0);
    }

    clear(x,y,w,h) {
        this.gl.clearDepth(1.0);
        this.gl.clearColor(0, 0, 0, 1.0);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    }

    fill(x,y,w,h) {
        this.gl.clearDepth(1.0);
        this.gl.clearColor(0, 0, 0, 1.0);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    }

    preDraw() {
        const gl = this.gl;
        const buffer = this.buffer;
    }

    toVert(x,y,w,h) {
        const ox = x / w - 0.5; //x/w -0.5;
        const oy = y / h + 0.5; //y/h + 0.5;

        return [
            -0.1 + ox, 0.1 + oy,
            0.1 + ox, 0.1 + oy,
            -0.1 + ox, -0.1 + oy,

            0.1 + ox, 0.1 + oy,
            0.1 + ox, -0.1 + oy,
            -0.1 + ox, -0.1 + oy
        ];
    }

    drawSprite(sprite) {
        const {r,g,b,a} = sprite.getColor();
        this.gl.uniform4f(this.color, r/255, g/255, b/255, a);

        this.gl.bufferData(
            this.gl.ARRAY_BUFFER,
            new Float32Array(this.toVert(sprite.x,sprite.y,sprite.w,sprite.h)), this.gl.STATIC_DRAW);

        this.gl.uniform2fv(this.location, [0,0]);
        this.gl.drawArrays(this.gl.TRIANGLES, 0, 6);
    }

    postDraw() {}
}
