/**
 * ImagesManager
 */
class ImagesManager {
    /**
     * Create instance
     */
    constructor() {
        this.images = new Map();
        this.imagesKeys = new Set(this.loadImagesKeys());
        this.initUI();
        this.initEvents();
        this.updateImages();
        this.selected = null;
    }

    initUI() {
        const element = (query) => document.querySelector(query);

        this.ui = {
            imageLoadButton: element('#image-load-button'),
            imageInput: element('#image-input'),
            imagesAlert: element('#images-alert'),
            imagesList: element('#images-list')
        }
    }

    initEvents() {
        const {imageLoadButton, imageInput} = this.ui;

        imageLoadButton.onclick = () => {
            imageInput.dispatchEvent(new MouseEvent('click'));
        };

        imageInput.onchange = () => {
            const file = imageInput.files[0];
            const reader = new FileReader();

            reader.onload = () => {
                this.saveImage(Date.now(), file.name, reader.result);
            };

            if (!file.type.match(/image\.*/)) {
                return;
            }

            reader.readAsDataURL(file);
        };
    }

    /**
     * Load ImageKeys
     * @returns {[]}
     */
    loadImagesKeys() {
        let imageKeys = localStorage.getItem('imagesKeys');

        if (imageKeys) {
            return imageKeys.split(',');
        } else {
            return [];
        }
    }

    saveImage(key, name, data) {
        this.hideAlert();
        this.imagesKeys.add(key);

        const keys = [...this.imagesKeys];
        localStorage.setItem('imagesKeys', keys.join(','));
        localStorage.setItem(key, JSON.stringify({name, data}));
        this.updateImages();
    }

    removeImage(key) {
        const imagesKeys = this.imagesKeys;
        if (imagesKeys.has(key)) {
            imagesKeys.delete(key);
            localStorage.setItem('imagesKeys', [...imagesKeys].join(','));
            localStorage.removeItem(key);
            this.updateImages();
        } else {
        }
    }

    updateImages() {
        const imagesKeys = this.imagesKeys;
        const {imagesList} = this.ui;

        if (imagesKeys.size === 0) {
            this.showAlert();
            this.selected = null;
            return;
        }

        imagesList.innerHTML = '';
        this.images.clear();
        this.selected = null;

        for (const key of imagesKeys) {
            const {name, data} = JSON.parse(localStorage.getItem(key));
            const image = new Image();
            const div = document.createElement('div');
            const p = document.createElement('p');
            const button = document.createElement('button');
            div.className = 'thumbnail';
            div.dataset.key = key;

            button.onclick = () => {
                div.parentNode.removeChild(div);
                this.removeImage(key);
            };

            div.onclick = () => {
                this.selected = this.images.get(key);
            };

            p.innerHTML = name;
            image.title = name;
            image.src = data;
            imagesList.appendChild(div);
            button.className = 'close';
            button.innerText = 'X';
            div.appendChild(button);
            div.appendChild(image);
            div.appendChild(p);

            this.images.set(key, image);
        }
    }

    /**
     * Get ImageKeys
     * @returns {[]}
     */
    getImagesKeys() {
        return this.imagesKeys;
    }

    /**
     * Show Alert
     */
    showAlert() {
        this.ui.imagesAlert.style.display = 'inline-block';
    }

    /**
     * Hide Alert
     */
    hideAlert() {
        this.ui.imagesAlert.style.display = 'none';
    }

    /**
     * Get selected image
     * @returns {Image|null}
     */
    getSelectedImage() {
        return this.selected;
    }

    /**
     * Check if some image is selected
     * @returns {boolean}
     */
    isImageSelected() {
        return this.selected !== null;
    }

    /**
     * Create
     * @returns {ImagesManager}
     */
    static create() {
        if (ImagesManager.singleton === undefined) {
            return ImagesManager.singleton = new ImagesManager();
        }
    }
}
