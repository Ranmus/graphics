/**
 * Engine class
 */
class Engine {
    constructor(settings) {
        this.settings = settings;
        this._layers = new Set();
        this._callBacks = new Set();
        this._sprites = new Set();
        this._run = false;
        this.resetDebug();
    }

    /**
     * Reset debug variables
     */
    resetDebug() {
        this._debug = {
            last: 0,
            fps: 0,
            frames: 0,
            accumulator: 0
        };
    }

    /**
     * Start engine
     *
     * @returns {Engine}
     */
    start() {
        if (this._run === false) {
            this._run = true;
            this.resetDebug();
            this.run();
        }
        return this;
    }

    /**
     * Stop engine
     *
     * @returns {Engine}
     */
    stop() {
        if (this._run === true) {
            this._run = false;
            this.resetDebug();
        }

        return this;
    }

    /**
     * Debug engine
     */
    debug() {
        const time = performance.now(),
            debug = this._debug;
        debug.delta = (time - debug.last);
        debug.last = time;
        debug.accumulator += debug.delta;
        debug.frames++;

        if (debug.accumulator > 1000) {
            debug.accumulator -= 1000;
            debug.fps = debug.frames;
            debug.frames = 0;
        }
    }

    /**
     * Run engine
     */
    run() {
        const loop = () => {
            this.debug();
            this.runCallbacks();
            this.drawLayers();

            if (this._run === false) {
                this.resetDebug();
                return;
            }

            requestAnimationFrame(loop);
        };

        loop();
    }

    /**
     * Get fps
     *
     * @returns {number}
     */
    get fps() {
        return this._debug.fps;
    }

    /**
     * Create layer
     *
     * @param name
     * @returns {Layer}
     */
    createLayer(name) {
        const layer = new Layer(this.settings);
        this._layers.add(layer);

        return layer;
    }

    /**
     * Draw layers
     */
    drawLayers() {
        for (const layer of this._layers) {
            layer.draw();
        }
    }

    /**
     * Set layers order
     * @param layers
     */
    setLayersOrder(layers) {
        for (const layer of layers) {
            if (this._layers.has(layer)) {
                this._layers.delete(layer);
            }
        }

        let z = this.settings.target.style.zIndex || 0;

        layers.map((layer) => {
            this._layers.add(layer);
            layer.setZ(++z);
        });
    }

    /**
     * Add callback
     * @param func
     * @returns {Engine}
     */
    addCallback(func) {
        if (this._callBacks.has(func) === false) {
            this._callBacks.add(func);
        }

        return this;
    }

    /**
     * Remove callback
     * @param func
     * @returns {Engine}
     */
    removeCallback(func) {
        if (this._callBacks.has(func)) {
            this._callBacks.delete(func);
        }

        return this;
    }

    runCallbacks() {
        for (const callback of this._callBacks) {
            callback();
        }
    }
}
