/**
 * This is main class
 * acts as facade api for user
 */
class GraphicsManager {
    constructor(options) {
        const {target} = options;
        target.style.width = `${options.width}px`;
        target.style.height = `${options.height}px`;
        this._engine = new Engine(options);
    }

    /**
     * Start engine
     * @returns {GraphicsManager}
     */
    start() {
        this._engine.start();
        return this;
    }

    /**
     * Stop engine
     * @returns {GraphicsManager}
     */
    stop() {
        this._engine.stop();
        return this;
    }

    /**
     * Check if engine is running
     * @returns {boolean}
     */
    isRunning() {
        return this._engine._run;
    }

    /**
     * Get current fps
     * @returns {*}
     */
    getFPS() {
        return this._engine.fps;
    }

    /**
     * Create new layer
     * @returns {Layer}
     */
    createLayer() {
        return this._engine.createLayer();
    }

    /**
     * Reorder layers
     * @param layers
     * @returns {GraphicsManager}
     */
    setLayersOrder(...layers) {
        this._engine.setLayersOrder(layers);
        return this;
    }

    /**
     * Add callback
     * @param func
     */
    addCallback(func) {
        this._engine.addCallback(func);
    }

    /**
     * Remove callback
     * @param func
     */
    removeCallback(func) {
        this._engine.removeCallback(func);
    }
}
