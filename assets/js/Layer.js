

/**
 * Layers class
 */
class Layer {
    constructor(settings) {
        const {target, context, width, height} = settings;
        const canvas = document.createElement('canvas');

        target.appendChild(canvas);
        canvas.setAttribute('width', width);
        canvas.setAttribute('height', height);
        canvas.style.width = `${width}px`;
        canvas.style.height = `${height}px`;
        canvas.style.position = 'absolute';

        this.canvas = canvas;
        this.width = width;
        this.height = height;

        if (context === '2d') {
            this.r = new CanvasRenderer(this.canvas);
        } else if (context === 'webgl') {
            this.r = new WebGLRenderer(this.canvas);
        } else {
            throw "Layer Exception: No valid context found!";
        }

        this._sprites = new Set();
    }

    /**
     * Set z index for ordering layers
     * @param z
     */
    setZ(z) {
        this.canvas.style.zIndex = z;
    }

    /**
     * Get graphics context
     * @returns {*|CanvasRenderingContext2D}
     */
    getContext() {
        return this.ctx;
    }

    /**
     * et associated renderer
     * @returns {CanvasRenderer|WebGLRenderer}
     */
    getRenderer() {
        return this.r;
    }

    /**
     * Get layer size
     * @returns {{width: *, height: *}}
     */
    getSize() {
        return {
            width: this.width,
            height: this.height
        }
    }

    /**
     * Add sprite to layer
     * @param sprite
     */
    addSprite(sprite) {
        this._sprites.add(sprite);
    }

    /**
     * Remove sprite from layer
     * @param sprite
     */
    removeSprite(sprite) {
        this._sprites.delete(sprite);
    }

    /**
     * Get all sprites
     * @returns {*[]}
     */
    getSprites() {
        return [...this._sprites];
    }

    /**
     * Set clear mode
     * @returns {Layer}
     */
    setClear() {
        this.fill = false;

        return this;
    }

    /**
     * Set fill color
     *
     * @param color
     * @returns {Layer}
     */
    setFill(color) {
        this.fill = color;

        return this;
    }

    /**
     * Clear layer
     */
    clear() {
        if (this.fill === false) {
            this.r.clear(0,0,this.width,this.height);
        } else {
            this.r.fillColor = this.fill;
            this.r.fill(0,0,this.width,this.height);
        }
    }

    /**
     * Draw all
     */
    draw() {
        this.clear();
        this.r.preDraw();

        // Draw sprites
        for (const sprite of this._sprites) {
            this.r.drawSprite(sprite);
        }

        this.r.postDraw();
    }
}





