/**
 * Point class
 */
class Point {
    /**
     * Create point
     * @param x
     * @param y
     */
    constructor(x, y) {
        this._x = x;
        this._y = y;
    }

    /**
     * Get position
     * @returns {{x: *, y: *}}
     */
    getPos() {
        return {
            x: this._x,
            y: this._y
        }
    }

    /**
     * Set position
     * @param x
     * @param y
     * @returns {Point}
     */
    setPos(x, y) {
        this._x = x;
        this._y = y;

        return this;
    }

    /**
     * @param x
     */
    set x(x) {
        this._x = x;
    }

    /**
     * @returns {*}
     */
    get x() {
        return this._x;
    }

    /**
     * @param y
     */
    set y(y) {
        this._y = y;
    }

    /**
     * @returns {*}
     */
    get y() {
        return this._y;
    }
}

class Rectangle extends Point {
    constructor(x, y, w, h) {
        super(x, y);

        this._w = w;
        this._h = h;
    }

    /**
     * Get size
     * @returns {{w: *, h: *}}
     */
    getSize() {
        return {
            w: this._w,
            h: this._h
        }
    }

    /**
     * Set size
     * @param w
     * @param h
     * @returns {Rectangle}
     */
    setSize(w, h) {
        this._w = w;
        this._h = h;

        return this;
    }

    /**
     * Get coordinates
     * @returns {{x: *, y: *, w: *, h: *}}
     */
    getCoords() {
        const pos = this.getPos();
        const size = this.getSize();
        return {
            x: pos.x,
            y: pos.y,
            w: size.w,
            h: size.h
        }
    }

    /**
     * @param w
     */
    set w(w) {
        this._w = w;
    }

    /**
     * @returns {*}
     */
    get w() {
        return this._w;
    }

    /**
     * @param h
     */
    set h(h) {
        this._h = h;
    }

    /**
     * @returns {*}
     */
    get h() {
        return this._h;
    }
}

class Object extends Rectangle {
    constructor(options) {
        const {x,y,w,h} = options;
        super(x,y,w,h);

        this._color = '#000';
        this._cssColor = 'rgba(0,0,0,1)';
    }

    /**
     * Get color
     *
     * @param color
     * @returns {Object}
     */
    setColor(color) {
        this._color = color;
        const {r,g,b,a} = color;
        this._cssColor = `rgba(${r},${g},${b},${a})`;

        return this;
    }

    /**
     * Get color
     * @returns {*}
     */
    getColor() {
        return this._color;
    }

    /**
     * get CSS color
     * @returns {string}
     */
    getCSSColor() {
        return this._cssColor;
    }
}

/**
 * Sprite class
 */
class Sprite extends Object {
    constructor(options) {
        let {x,y,w,h} = options;

        super({
            x:x||0,
            y:y||0,
            w:w||1,
            h:h||1
        });

        if (options.color) {
            this.setColor(options.color);
        }

        this._image = null;
    }

    /**
     * Set image
     * @param image
     * @returns {Sprite}
     */
    setImage(image) {
        this._image = image;

        return this;
    }

    /**
     * Get image
     * @returns {null|*}
     */
    getImage() {
        return this._image;
    }

    /**
     * Check if sprite has image
     * @returns {boolean}
     */
    hasImage() {
        return this._image !== null;
    }

    /**
     * @param image
     */
    set image(image) {
        this._image = image;
    }

    /**
     * @returns {*|null}
     */
    get image() {
        return this._image;
    }
}
